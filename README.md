# DrumKit

I wanted to show much cooler **DrumKit** web site.

This web site built :
  - HTML
  - CSS
  - JAVASCRIPT
  - DOM

So the way that this works is that we've got a number of keys here that represent different drums in a typical drum set. And when you click on any of these buttons then you will get the corresponding sound of the drum. In addition, you can also use the keyboard.

You can see this page [here](https://aselt.gitlab.io/drumkit/).

Git commands to download this project:
```
git clone https://gitlab.com/Aselt/drumkit.git or SSH here
cd DrumKit (Name-of-Project)
```
